import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JOptionPane;

public class FlujoVentasApp {

	public static void main(String[] args) {
		
		// Creaci�n de una cantidad de productos con un precio aleatorio
		int cantidad = Integer.parseInt(JOptionPane.showInputDialog("Introduce una cantidad"));
		
		ArrayList<Integer> listaCompra = new ArrayList<Integer>();
		
		for (int i = 0; i < cantidad; i++) {
		
			listaCompra.add((int) (Math.random() * (20 - 1 + 1) + 1));
			
		}
		
		boolean comprarMas = true;
		
		// Bucle 'do' que a�ade productos si el usuario as� lo desea
		do {
			
			String respuesta = JOptionPane.showInputDialog("�Deseas a�adir alg�n otro producto?");
			
			if (respuesta.equalsIgnoreCase("Si")) {
				
				listaCompra.add((int) (Math.random() * (20 - 1 + 1) + 1));
				
				JOptionPane.showMessageDialog(null, "�Producto a�adido!");
				
				comprarMas = true;
				
				cantidad++;
				
			} else {
				
				comprarMas = false;
				
			}
			
		} while (comprarMas);
		
		// Iterador que recorre el array y suma los valores
		Iterator<Integer> it = listaCompra.iterator();
		
		int num;
		int suma = 0;
		
		while (it.hasNext()) {
			
			num = it.next();
			suma += num;
			
		}
		
		double IVA = Integer.parseInt(JOptionPane.showInputDialog("Introduce un IVA"));
		double sumaIVA = 0;
		
		// Aplicaci�n sobre el precio final del IVA introducido por el usuario
		if (IVA == 4) {
			
			sumaIVA = suma + (suma * 0.4);
			
		} else if (IVA == 21) {

			sumaIVA = suma + (suma * 0.21);
			
		}
		
		// Muestra y c�lculo de la cantidad a pagar y la cantidad a devolver al cliente
		double pago = Integer.parseInt(JOptionPane.showInputDialog("Cantidad a pagar: " + sumaIVA + "\n" + "Introduce el pago"));
		double cambio = 0;
		
		if (pago > sumaIVA) {
			
			cambio = pago - sumaIVA;
			
		}
		
		JOptionPane.showMessageDialog(null, "IVA APLICADO: " + IVA + "%" + "\n"
		+ "Precio total bruto: " + suma + "�" + "\n"
				+ "Precio total mas IVA: " + sumaIVA + "�" + "\n"
		+ "N�mero de art�culos comprados: " + cantidad + "\n"
				+ "Cantidad pagada: " + pago + "�" + "\n"
		+ "Cambio a devolver al cliente: " + cambio + "�");

	}

}
