import java.util.Enumeration;
import java.util.Hashtable;

import javax.swing.JOptionPane;

public class StockTiendaApp {

	public static void main(String[] args) {
		
		// Creaci�n de un 'hashtable' de diez productos con nombre y precio
		Hashtable<String, Double> contenedor = new Hashtable<String, Double>();
		
		contenedor.put("A", 1.00);
		contenedor.put("B", 2.00);
		contenedor.put("C", 3.00);
		contenedor.put("D", 4.00);
		contenedor.put("E", 5.00);
		contenedor.put("F", 6.00);
		contenedor.put("G", 7.00);
		contenedor.put("H", 8.00);
		contenedor.put("I", 9.00);
		contenedor.put("J", 10.00);
		
		a�adir(contenedor);
		
		mostrar(contenedor);
		
	}
	
	// M�todo que crea un producto nuevo si el usuario as� lo desea
	public static void a�adir (Hashtable<String, Double> contenedor) {
		
		String productoNuevo = JOptionPane.showInputDialog("�Deseas a�adir un producto a la base de datos?");
				
		if (productoNuevo.equalsIgnoreCase("Si")) {
							
			boolean bucle = true;
					
			// Bucle 'do/while' que permite al usuario seguir a�adiendo m�s productos si as� lo desea
			do {
						
				String producto = JOptionPane.showInputDialog("Introduce el nombre del producto");
				String precio = JOptionPane.showInputDialog("Introduce el precio del producto");
						
				contenedor.put(producto, Double.parseDouble(precio));
				
				String respuesta = JOptionPane.showInputDialog("�Deseas a�adir otro producto a la base de datos?");
						
				if (respuesta.equalsIgnoreCase("Si")) {
							
					bucle = true;
							
				} else {
							
					bucle = false;
							
				}
						
			} while (bucle);
					
		}
		
	}
	
	// M�todo que da la elecci�n al usuario de consultar un producto en concreto o toda la lista
	public static void mostrar (Hashtable<String, Double> contenedor) {
		
		String mostrar = JOptionPane.showInputDialog("�Deseas consultar un producto en concreto o consultar toda la lista?");
		
		if (mostrar.equalsIgnoreCase("Consultar producto")) {
			
			String nombreProducto = JOptionPane.showInputDialog("Introduce el nombre del producto");
			
			System.out.println(contenedor.get(nombreProducto));
			
		} else if (mostrar.equalsIgnoreCase("Consultar lista")) {
			
			Enumeration<Double> enumeration = contenedor.elements();
			Enumeration<String> llaves = contenedor.keys();
			
			while (enumeration.hasMoreElements()) {
				
				System.out.println(llaves.nextElement());
				System.out.println(enumeration.nextElement());
				
				System.out.println();
				
			}
			
		}
		
	}

}
