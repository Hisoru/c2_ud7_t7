import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Scanner;

public class NotaMediaApp {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		// Arrays de notas
		int[] notasDaniel = {5, 6, 7, 8};
		int[] notasCristian = {9, 8, 7, 9};
		
		// C�lculo de las medias
		double mediaDaniel = calculoMedia(notasDaniel);
		double mediaCristian = calculoMedia(notasCristian);
		
		Hashtable<String, Double> contenedor = new Hashtable<String, Double>();
		
		// Almacenamiento de las medias en un diccionario de datos
		contenedor.put("Daniel", mediaDaniel);
		contenedor.put("Cristian", mediaCristian);
		
		System.out.println(contenedor);

	}
	
	// M�todo que calcula la media de las notas del array
	public static double calculoMedia (int[] notas) {
		
		double media = 0;
		
		for (int i = 0; i < notas.length; i++) {
			
			media += notas[i];
			
		}
		
		double resultado = media / notas.length;
		
		return resultado;
		
	}
	
}
