import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;

import javax.swing.JOptionPane;

public class VentasStockApp {

	public static void main(String[] args) {
		
		String seleccion = JOptionPane.showInputDialog("Selecciona una opci�n" + "\n" + "1. Comprar" + "\n" + "2. Consultar");
		
		switch (seleccion) {
		
			case "1":
				comprar();
				break;
				
			case "2":
				consultar();
				break;
			
			default:
				JOptionPane.showMessageDialog(null, "No has introducido una opci�n correcta");
	
		}

	}
	
	public static void comprar () {
			
		// Creaci�n de una cantidad de productos con un precio aleatorio
		int cantidad = Integer.parseInt(JOptionPane.showInputDialog("Introduce una cantidad"));
			
		ArrayList<Integer> listaCompra = new ArrayList<Integer>();
			
		for (int i = 0; i < cantidad; i++) {
			
			listaCompra.add((int) (Math.random() * (20 - 1 + 1) + 1));
				
		}
			
		boolean comprarMas = true;
			
		// Bucle 'do' que a�ade productos si el usuario as� lo desea
		do {
				
			String respuesta = JOptionPane.showInputDialog("�Deseas a�adir alg�n otro producto?");
				
			if (respuesta.equalsIgnoreCase("Si")) {
					
				listaCompra.add((int) (Math.random() * (20 - 1 + 1) + 1));
					
				JOptionPane.showMessageDialog(null, "�Producto a�adido!");
					
				comprarMas = true;
					
				cantidad++;
					
			} else {
					
				comprarMas = false;
					
			}
				
		} while (comprarMas);
			
		// Iterador que recorre el array y suma los valores
		Iterator<Integer> it = listaCompra.iterator();
			
		int num;
		int suma = 0;
			
		while (it.hasNext()) {
				
			num = it.next();
			suma += num;
				
		}
			
		double IVA = Integer.parseInt(JOptionPane.showInputDialog("Introduce un IVA"));
		double sumaIVA = 0;
			
		// Aplicaci�n sobre el precio final del IVA introducido por el usuario
		if (IVA == 4) {
				
			sumaIVA = suma + (suma * 0.4);
				
		} else if (IVA == 21) {
	
			sumaIVA = suma + (suma * 0.21);
				
		}
			
		// Muestra y c�lculo de la cantidad a pagar y la cantidad a devolver al cliente
		double pago = Integer.parseInt(JOptionPane.showInputDialog("Cantidad a pagar: " + sumaIVA + "\n" + "Introduce el pago"));
		double cambio = 0;
			
		if (pago > sumaIVA) {
				
			cambio = pago - sumaIVA;
				
		}
			
		JOptionPane.showMessageDialog(null, "IVA APLICADO: " + IVA + "%" + "\n"
		+ "Precio total bruto: " + suma + "�" + "\n"
				+ "Precio total mas IVA: " + sumaIVA + "�" + "\n"
		+ "N�mero de art�culos comprados: " + cantidad + "\n"
				+ "Cantidad pagada: " + pago + "�" + "\n"
		+ "Cambio a devolver al cliente: " + cambio + "�");
		
	}
	
	public static void consultar () {
		
		// Creaci�n de un 'hashtable' de diez productos con nombre y precio
		Hashtable<String, Double> contenedor = new Hashtable<String, Double>();
				
		contenedor.put("A", 1.00);
		contenedor.put("B", 2.00);
		contenedor.put("C", 3.00);
		contenedor.put("D", 4.00);
		contenedor.put("E", 5.00);
		contenedor.put("F", 6.00);
		contenedor.put("G", 7.00);
		contenedor.put("H", 8.00);
		contenedor.put("I", 9.00);
		contenedor.put("J", 10.00);
				
		a�adir(contenedor);
				
		mostrar(contenedor);
				
	}
			
	// M�todo que crea un producto nuevo si el usuario as� lo desea
	public static void a�adir (Hashtable<String, Double> contenedor) {
				
		String productoNuevo = JOptionPane.showInputDialog("�Deseas a�adir un producto a la base de datos?");
						
		if (productoNuevo.equalsIgnoreCase("Si")) {
									
			boolean bucle = true;
							
			// Bucle 'do/while' que permite al usuario seguir a�adiendo m�s productos si as� lo desea
			do {
								
				String producto = JOptionPane.showInputDialog("Introduce el nombre del producto");
				String precio = JOptionPane.showInputDialog("Introduce el precio del producto");
								
				contenedor.put(producto, Double.parseDouble(precio));
						
				String respuesta = JOptionPane.showInputDialog("�Deseas a�adir otro producto a la base de datos?");
								
				if (respuesta.equalsIgnoreCase("Si")) {
									
					bucle = true;
									
				} else {
									
					bucle = false;
									
				}
								
			} while (bucle);
							
		}
				
	}
			
	// M�todo que da la elecci�n al usuario de consultar un producto en concreto o toda la lista
	public static void mostrar (Hashtable<String, Double> contenedor) {
				
		String mostrar = JOptionPane.showInputDialog("�Deseas consultar un producto en concreto o consultar toda la lista?");
				
		if (mostrar.equalsIgnoreCase("Consultar producto")) {
					
			String nombreProducto = JOptionPane.showInputDialog("Introduce el nombre del producto");
					
			System.out.println(contenedor.get(nombreProducto));
					
		} else if (mostrar.equalsIgnoreCase("Consultar lista")) {
					
			Enumeration<Double> enumeration = contenedor.elements();
			Enumeration<String> llaves = contenedor.keys();
					
			while (enumeration.hasMoreElements()) {
						
				System.out.println(llaves.nextElement());
				System.out.println(enumeration.nextElement());
						
				System.out.println();
						
			}
					
		}
		
	}

}
